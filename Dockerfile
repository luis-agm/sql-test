FROM mysql:latest

ENV MYSQL_DATABASE company

COPY ./sql-scripts/database.sql /docker-entrypoint-initdb.d/