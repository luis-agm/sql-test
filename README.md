# Simple database to test SQL queries

## Run with docker CLI

* Create the mysql instance with docker:

    `$ docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:latest`

* Import the database:

    `$ docker exec -i some-mysql sh -c 'exec mysql -uroot -p"my-secret-pw"' < ./sql-scripts/database.sql`

* Get into the CLI (enter passoword set above when prompted):

    `$ docker exec -it some-mysql mysql -p`

* Do stuff:
    `mysql> SELECT * FROM Customers;`

## Run with Dockerfile

* Build the image:

    `$ docker build some-mysql .`

* Run the image:

    `$ docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d some-mysql`

* Get into the CLI (enter passoword set above when prompted):

    `$ docker exec -it some-mysql mysql -p`

* Do stuff:
    `mysql> SELECT * FROM Customers;`

## Note

If you want to connect to the database from an app, workbench, etc. you must expose the port when running the image:

`$ docker run --name some-mysql -p3306:3306 -e MYSQL_ROOT_PASSWORD=my-secret-pw -d some-mysql`
