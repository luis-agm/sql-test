CREATE DATABASE CustomerTest;

USE CustomerTest;

CREATE TABLE Customers (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    CustomerName varchar(255)
);

CREATE TABLE Orders (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    CustomerId int
);

INSERT INTO Customers (CustomerName) VALUES ('James');
INSERT INTO Customers (CustomerName) VALUES ('Mark');
INSERT INTO Customers (CustomerName) VALUES ('Oscar');
INSERT INTO Customers (CustomerName) VALUES ('Jane');
INSERT INTO Customers (CustomerName) VALUES ('Jojoto');

INSERT INTO Orders (CustomerId) VALUES ('2');
INSERT INTO Orders (CustomerId) VALUES ('4');